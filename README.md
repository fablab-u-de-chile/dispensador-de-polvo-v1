# Dispensador de polvo vertical

Un dispensador de polvo impreso en 3D, funciona mediante un tornillo Auger que mueve los polvos dentro del contenedor, mezclándolos y empujándolos hacia la boquilla de salida. La forma del tornillo lo hace especialmente efectivo para polvos gruesos o con mucho roce, pues se tienden a aglomerar o apelmazar, como es el caso de los residuos pulverizados. Este dispensador no funciona correctamente con polvos finos que fluyen facilmente, para eso puede ser mejor [ESTE](https://gitlab.com/fablab-u-de-chile/dispensador-de-polvo-horizontal) proyecto.

<img src="/img/isopowdv.png" width="300">

## Atributos

- Flujo de x cc/s.
- Contenedor de n cc.
- Actuado por motor DC.
- Para anclar en perfiles V-Slot con pernos M5.

[VIDEO](https://youtu.be/ko5EFEyb6T4) del sistema funcionando.

## Cómo construirlo

- [LISTADO](parts.md) de partes, piezas y herramientas. 
- [Partes CAD](parts/).
- [ELECTRÓNICA](elec.md).
- Instrucciones de ensamble.

## Trabajo futuro

- ...

### Ponte en contacto

Este actuador es parte de un [proyecto](https://gitlab.com/fablab-u-de-chile/biomixer) más grande desarrollado por el FabLab U de Chile. Puedes ver más de nuestra labor en:

- [Página Web](http://www.fablab.uchile.cl/)
- [Instagram](https://www.instagram.com/fablabudechile/?hl=es-la)
- [Youtube](https://www.youtube.com/channel/UC4pvq8aijaqn5aN02GiDwFA)

## Licencia

Este trabajo está licenciado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png




