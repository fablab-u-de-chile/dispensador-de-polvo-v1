# Partes y piezas

## Partes comerciales

 ITEM              | Cantidad
 ---------------------------   | ------------
 Motor DC 12V 60RPM | 1
 Golilla M5 | 1
 Perno Parker M3x12 | 1
 M3x8 perno avellanado | 2
 M3 tuerca | 1
 Tubo de acrílico 45 mm DE | 1

 Se debe cortar una sección de tubo de 120 mm. Para el anclaje del dispensador se utiliza además 2 pernos M5x15 y 2 tuercas martillos de V-Slot.

## Partes impresas

Los archivos .step y .stl los puedes encontrar en la [carpeta de partes](parts/).

 ITEM                   | Cantidad
 ---------------------------   | ------------
 Tapa deslizante| 1
 SoporteMotor | 1
 Base| 1
 Tornillo Auger| 1
 Tapa | 1

## Herramientas

 ITEM |                  
 --------------------------- |  
 Sierra |
 Llave allen 2.5 mm |
 Llave allen 2 mm |


